

 void Tsyn_map::Read_Velosity(int kl,AnsiString flnm)

 {//n?eouaaai aaiiua nei?inoe ia iiaa?oiinoe enoi?ieea

  int i,j,l,k,yr,mon,dy,kol_st=0,nx,ny,car_rot=2184;

  int *imf;

  float *speed,*velos;

  float yrf,doy,spd,pol,dyf,kpf,fm[20];

  double dat[300];

  char st1[400];

  FILE *fl_in;

  AnsiString s1,s2,fname,path,cur_dir,mask="cr_vs*.fit";

  D_GC date1;

  double jd,t,t1853=2398167.43541622,t1970=2440587.5;

  D_GC d0,d1970,d2000;

  float dph0=0.0,L0f,*vel_map,*br_map;

  float L01=0,B1,P1;

  double *date;

  TSearchRec sr;

  int iAttributes =faAnyFile;


// flnm="c:\\STOP\\Best1\\Map\\170823\\cr_vs_170823.fit";



   if(!FileExists(flnm) || flnm.IsEmpty())

   { path="c:\\STOP\\Best1\\Map\\";


     if(date_calc.y>2000)

     {s1=IntToStr(date_calc.y-2000);

      if(date_calc.m<10) s1+="0"; s1+=IntToStr(date_calc.m)+"";
      if(date_calc.d<10) s1+="0"; s1+=IntToStr((int)date_calc.d);
      path="c:\\STOP\\Best1\\Map\\_"+s1;

     }

     else

       path="c:\\STOP\\Best1\\Map\\";


    if(DirectoryExists(path));
      SelectDirectory(path,TSelectDirOpts()  << sdPrompt,i);
   }
   else
    {  path=ExtractFilePath(flnm) ;
       cur_dir=GetCurrentDir();
       if(cur_dir != path)
          SelectDirectory(path,TSelectDirOpts()  << sdPrompt,i);
    }

   if(FindFirst(mask, iAttributes, sr)==0) //anou oaeea oaeeu
    flnm=sr.Name;

   FindClose(sr);



 if(FileExists(flnm) & flnm.Pos(".fit")>0 )

   fname=flnm;
  else
  {
   Pfss_Form->OpenDialog1->Filter =
     "fit (*.fit)|*.fit|txt (*.txt)|*.txt|All (*.*)|*.*";
    Pfss_Form->OpenDialog1->DefaultExt="*.txt";
   if(DirectoryExists(path))
     Pfss_Form->OpenDialog1->InitialDir=path;

   if (Pfss_Form->OpenDialog1->Execute())
     s1=Pfss_Form->OpenDialog1->FileName;
    if((fl_in=fopen(s1.c_str(), "r"))==NULL)
     { s2="not file "+s1;
        Application->MessageBox(s2.c_str(), "error", MB_OK);
       return;
     }
     fname=s1;
  }

  //aun?eouaaai aaoo   ec eiaie
  i=fname.Pos(".fit");
  if(i>0)
   { s2=fname.SubString(i-6,6);
     s2=s2.Insert(" ",5);
     s2=s2.Insert(" ",3);
     k=get_fm(s2,fm);
     if(k==3)
      { yr=2000+fm[0];
        mon=fm[1]; dy=fm[2];
        d0.y=yr; d0.m=mon; d0.d=0.3+dy;
        GELIOG_K(d0,&L01,&B1,&P1);
      }
   }


 //n?eouaaiea fit oaeea oeia
   nx=720,ny=360;


   //auaaeyai iaiyou

   (float *)vel_map=new float[(nx+2)*(ny+2)];


   int k1=Read_Calculation_Fits(0,fname,&nx,&ny,vel_map);
   if(k1<0)
     return;
   car_rot=n_ker;

  //iaaieoiia iiea
  (float *)br_map=new float[(nx+2)*(ny+2)];
  fname=StrTran(fname,"cr_vs","cr_brss");
  k1=Read_Calculation_Fits(0,fname,&i,&j,br_map);
   if(k1<0)
     return;

 //ia?anoaaeyai ianneau
  float *swap,mnv,mxv,mnb,mxb,f;
  mnv=mnb=1e6; mxv=mxb=-1e6;

  (float *)swap=new float[(nx+2)*(ny+2)];
   for (i = 0; i < nx; i++)
       for (j = 0; j < ny; j++)
         swap[j * nx + i]=vel_map[i*ny+j];
   for (i = 0; i < nx; i++)
       for (j = 0; j < ny; j++)
         {vel_map[j * nx + i]=swap[j * nx + i];
          f=vel_map[j * nx + i];
          if(f<mnv) mnv=f;
          if(f>mxv) mxv=f;
         }
   for (i = 0; i < nx; i++)
       for (j = 0; j < ny; j++)
         swap[j * nx + i]=br_map[i*ny+j];

   for (i = 0; i < nx; i++)
       for (j = 0; j < ny; j++)
        { br_map[j * nx + i]=swap[j * nx + i];
          f=br_map[j * nx + i];
          if(f<mnb) mnb=f;
          if(f>mxb) mxb=f;
        }
  delete[] swap;


   struct Grid3D *grid3d;
  (Grid3D*) grid3d = new Grid3D;
  grid3d->np = nx; //aieaioia
  grid3d->nt = ny; //oe?ioa
  grid3d->nr =NSTEP; //?aaeon
  grid3d->nr =30; //?aaeon
  grid3d->ph = (float *)(calloc(np+2, sizeof(float)));
  grid3d->th = (float *)(calloc(nt+2, sizeof(float)));
  grid3d->time = (double *)(calloc(nx+2, sizeof(double)));
  grid3d->b0 = (float *)(calloc(nx+2, sizeof(float)));

   double t_frst, t_last, t_cmp;
   L0f=360;
   t_cmp= carlon2time(car_rot, L0f); //a?aiy aua?aiiiai aiy a nae io 1970 a.


     for (i = 0; i < nx; i++) {
           grid3d->ph[i] = (i + 0.5) / nx * 2 * M_PI - dph0 / 2.0 * DTOR;
           grid3d->time[i] = carlon2time(car_rot,  grid3d->ph[i] / DTOR); //a?aiy io ia?aea iai?ioa ai +27.2753
        // Time  // CMP time of each subearth point (since 1977.01.01) [NP]
            grid3d->b0[i] =time2solarb(grid3d->time[i]);			// Get b angle
        }
     grid3d->b0[0] =time2solarb(grid3d->time[nx/2]+t_cmp);			// Get b angle



   float delv=mxv-mnv,delb=mxb-mnb;

  for (int j = 0; j < ny; j++)
        grid3d->th[j] =  ((ny - j - 0.5) / ny * M_PI);



   D_GC d1,d2,d_first;

   double tstep=14400;

   double daysinadv=5.0;


   int size=tslen*3;

   (double*) date = new double[tslen+10];			// Time series at 1AU

   (int*) imf = new int[size+10]; // Imf polarity at 1AU

   (float*) speed =  new float[size+5];  // Speed at 1AU


       //iiiaio oaeouaai (iineaaiaai aiy)

       L0f=360;

       t_cmp =carlon2time(car_rot, L0f);

        jd=t1970+t_cmp/86400;
        d1=GC_JD(jd);
        daysinadv=12;
        t_last = t_cmp + (daysinadv + 0.5+4) * SECSINDAY; //a?aiy
        t_last = (int)(t_last / tstep) * tstep;
        t_frst =t_cmp - (daysinadv -1.5) * SECSINDAY; //a?aiy

        jd=t1970+t_frst/86400;
        date_first=d_first=GC_JD(jd);

        jd=t1970+t_last/86400;
        d2=GC_JD(jd);

        tstep=(t_last-t_frst)/tslen;


        for (i = 0; i < tslen; i++)
        {    //eioa?aae a?aiaie 20 aiae ?a?ac 4 ?ana
            date[i] = t_last - (tslen - i - 1) * tstep;  // Time series at 1AU
         }

   float L0dr,scale_r=1.3; //1.5 ano?iiiie?aneea aaeieou

     L0dr=L0f;

     if(fabs(L01-L0f)>1)

      L0dr=L01;

     int kl_save=1,cl_sh=28;


     s1=IntToStr((int)L0dr)+"           "+IntToStr(cl_sh);

     if(kl!=1) //ec oaeou aiy

     { InputQuery("input L0"," L0 central meridian  and color scheme (21-30)",s1);
       k=get_fm(s1,fm);
       L0dr=fm[0];
       if(k>1)
         cl_sh=fm[1];

     }


      //n?eouaaai ianeea nei?inoe ia 1AU aey a?aoeea

     int n_speed=0,xw1,xw2,yw1,yw2;

     float speed1[300],imf1[300];

     double spdate[300],jd0,jdsp1,jdsp2;

     AnsiString flnm_speed;

     flnm_speed=StrTran(fname,"cr_vs","speed");

     flnm_speed=StrTran(flnm_speed,"cr_brss","speed");

     flnm_speed=StrTran(flnm_speed,".fit",".dat");

     if(FileExists(flnm_speed))

       n_speed=Pfss_Form->Plot_map->Read_Speed_Imf(0,flnm_speed,spdate,speed1,imf1);



   do{

     //ii?aaaeyai aaeenoe?aneo? nei?inou

    Wsa_3d(kl_save,grid3d, vel_map, br_map, date, speed, imf, tslen,scale_r,L0dr,cl_sh);
    if(n_speed>0)
     {//no?iei nei?inou ia a?aoeea
      xw1=300; xw2=Pfss_Form->Image1->Width-100; yw1=Pfss_Form->Image1->Height-170; yw2=Pfss_Form->Image1->Height-30;
      t_cmp =carlon2time(car_rot, L0dr);//a?aiy a nae n 1970 a
      jd0=jd=t1970+t_cmp/86400;
      jdsp1=jd0-0.8;  jdsp2=jd0+3.5;
      Pfss_Form->Plot_map->Plot_Speed(Pfss_Form->Image1,n_speed,spdate,speed1,xw1,yw1,xw2,yw2,jd0,jdsp1,jdsp2);
     }
    kl_save=0;


     int id;
     if(kl==1)
      id=ID_YES;
     else
      id=Application->MessageBox( "Avtomat 3d image (Yes)  CONTINUE by hand (NO)  Break (Cancel)","Iino?iaiea aeaa?aii", MB_YESNOCANCEL);
     if(id==ID_CANCEL)
       return;
      if(id==ID_YES)
      { int nom=0;
        for(float l0=L0dr+6; l0>=L0dr-55; l0=l0-3)
        { Wsa_3d(kl_save,grid3d, vel_map, br_map, date, speed, imf, tslen,scale_r,l0,cl_sh);
          if(n_speed>0)
            {//no?iei nei?inou ia a?aoeea
              xw1=300; xw2=Pfss_Form->Image1->Width-100; yw1=Pfss_Form->Image1->Height-170; yw2=Pfss_Form->Image1->Height-30;
              jd=jd0-(l0-L0dr)/360*27.2753;
              Pfss_Form->Plot_map->Plot_Speed(Pfss_Form->Image1,n_speed,spdate,speed1,xw1,yw1,xw2,yw2,jd,jdsp1,jdsp2);
             }



            //caiiieiaai jpg
           AnsiString  sjpg="v3d_";
           if(nom<10) sjpg+="0";
           sjpg+=IntToStr(nom)+".jpg";
           TJPEGImage *jp = new TJPEGImage();

           try
            {    jp->Assign(Pfss_Form->Image1->Picture->Graphic);
              jp->SaveToFile(sjpg.c_str());
             }
            __finally
           { delete jp;}
          nom++; 
       }//l0

       break;
      }//ID_NO

     s1=IntToStr((int)L0dr)+"           "+IntToStr(cl_sh);
     if(!InputQuery("input L0"," L0 central meridian  and color scheme (21-30)",s1))
      break;


     k=get_fm(s1,fm);

     L0dr=fm[0];
     if(k>1)
      cl_sh=fm[1];

   }while(1<2);

  delete[] vel_map;
  delete[] br_map;
  delete grid3d;
  delete[] date;
  delete[] imf;
  delete[] speed;

  Pfss_Form->Label0->Caption="http://gifmaker.me";
  Pfss_Form->Label0->Update();



   Screen->Cursor = 0;



 }




void Tsyn_map::Wsa_3d(int kl_save,struct Grid3D *grid3d, float *v_s, float *brss_s, double *date,float *speed, int *imf, int tslen,float scale_r,float L0d,int cl_sh)

{ //au?eneaiea nei?inoe n 3d

    int i, j,size,k,k1,key_out=0;
    int np = grid3d->np;
    int nt = grid3d->nt;
    int nr = grid3d->nr;

    double *t0;
    double *t1au;
    float *v0, *b0, *v1au, *b1au;
    float *vrq,*vrfi;
    AnsiString s1;
    FILE *fl_out;
    AnsiString flnm_out;
    float f,rr,al,lng,lat,aa,bb,B0f;
    flnm_out="vv_eclipcic.txt";

    (double*) t0=new double[np+1];
    (double*) t1au=new double[np+1];
    (float*)  v0=new float[np+1];
    (float*)  b0=new float[np+1]; //oaie B0
    (float*)  v1au=new float[np+1];
    (float*)  b1au=new float[np+1];

    (float*)  vrq=new float[(nr+1)*(nt+1)+10];
    (float*)  vrfi=new float[(nr+1)*(np+1)+10];

     float *v2d,*b2d; //ianneau r x np aey ?aeiino?oeoee a ieineinoe
     size=(np+1)*(nr+1)+10;
     (float*)  v2d=new float[size];
     (float*)  b2d=new float[size];


    for (i = 0; i < np; i++)
        t0[i] = grid3d->time[np - 1 - i];  //ia?aai?a?eaaai ii a?aiaie

    int n_ecl;
    B0f=grid3d->b0[0];
    n_ecl=nt/2;
     if(fabs(B0f)<8)
      n_ecl=nt/2-B0f/180*nt;

    // Make date double
//    double date_tmp[tslen];
//    for (i = 0; i < tslen; i++) date_tmp[i] = date[i];
    float *speed_tmp;
    int *imf_tmp;

    if(kl_save==1)
     { flnm_out="vv_3d.txt";
       fl_out=fopen(flnm_out.c_str(), "w");
     }

    if(L0d>360) L0d=L0d-360;
    if(L0d<0)   L0d=L0d+360;


    for (j = 0; j < nt; j++) //oe?ioa
    {  //ia?aai?a?eaaai ii a?aiaie
        for (i = 0; i < np; i++)
         { v0[i] = v_s[j * np + np - 1 - i];
           b0[i] = brss_s[j * np + np - 1 - i];
          }

       lat=180.0*j/nt;
       //i?aia?aciaaiea ia ea?eiaoiiianeie naoea n o?aoii auno?uo iioie
        propagate2d(np,nr,t0, v0, b0, t1au, v1au, b1au,v2d,b2d,scale_r);

        speed_tmp = speed + j * tslen;
        imf_tmp = imf + j * tslen;
      //i?aia?aciaaiea ia ea?eaioiiianeie naoea n o?aoii auno?uo iioie
     //  fixts2d(t1au, v1au, b1au, np, date, speed_tmp, imf_tmp, tslen);
     //caiiieiaai oaee

     lng=360.0*j/nt;

       if(j==n_ecl &  key_out==0) //caiiieiaai e auaiaei ia ye?ai ?ac?ac ii ?aaeono e aieaioa aey L0
        {
        //auaiaei nei?inou a oaee
         int l_shift=-90;
          for(int ii=0;ii<nr;ii++)
            for(int jj=0;jj<np;jj++)
             { float kf=jj+(-L0d+l_shift)/360*np;  //iiaai? ia aieaioo
               if(kf<0) kf=kf+np;
               if(kf>=np) kf=kf-np;
               k=kf;
               int jj2=k+1;
               bb=kf-k; aa=1.0-bb;
               if(k<0)  k=np+k;  if(k<0) k=np+k;  if(k<0) k=np+k;
               if(k>=np) k=k-np; if(k>=np) k=k-np; if(k>=np) k=k-np;
               if(jj2<0) jj2=np+jj2;   if(jj2<0) jj2=np+jj2;   if(jj2<0) jj2=np+jj2;
               if(jj2>=np) jj2=jj2-np; if(jj2>=np) jj2=jj2-np; if(jj2>=np) jj2=jj2-np;
               float f2=*(v2d+jj2*nr+ii);
               f=*(v2d+k*nr+ii)*aa+bb*f2;
               rr=RS+scale_r*ii*PROPDIST / nr;
               al=grid3d->ph[k];
            //   fprintf(fl_out," %9.2f   %9.3f %9.1f \n",rr,al,f);
               *(vrfi+jj*nr+ii)=f;
             }
          key_out=1;
        }//j=L0d

        s1="th: "+IntToStr(j)+" ";
        Pfss_Form->Label0->Caption=s1;
        Pfss_Form->Label0->Update();
        Pfss_Form->Print_Mem(0);
       //?ac?ac ii ?aaeono e oe?ioa
        k=(-L0d)/360*np;
        if(k<0) k=np+k;   if(k<0) k=np+k;
        if(k>=np) k=k-np; if(k>=np) k=k-np;
        for(int ii=0;ii<nr;ii++)
        { int jecl=j+(nt/2-n_ecl);
          if(jecl<0) jecl=0; if(jecl>nt-1) jecl=nt-1;
          *(vrq+jecl*nr+ii)=*(v2d+k*nr+ii);
        }

        //auaiaei nei?inou a oaee
        if(kl_save==1)
          for(int ii=0;ii<nr;ii++)
            for(int jj=0;jj<np;jj++)
             { f=*(v2d+jj*nr+ii);
               rr=RS+scale_r*ii*PROPDIST / nr;
               al=grid3d->ph[jj]*180/Pi;
               fprintf(fl_out," %7.3f %7.2f  %9.3f %9.2f \n",rr,lat,al,f);
             }

    }//j oe?ioa

   if(kl_save==1)
    fclose(fl_out);

   //?ac?ac ii ?aaeono e oe?ioa
   AnsiString title,st2,s2;
   int yr,mn,dy;

   st2=DateToStr(Date());
   int i1=st2.Pos(".");
   if(i1>0) {st2=st2.Delete(i1,1); st2=st2.Insert(" ",i1);}
   i1=st2.Pos(".");
   if(i1>0) {st2=st2.Delete(i1,1); st2=st2.Insert(" ",i1);}
   sscanf(st2.c_str(),"%d %d %d",&dy,&mn,&yr);
   if(dy>31 ) { yr=i1; yr=dy;dy=i1; }
   if(yr<10) yr=yr+2000;
   if(yr<100) yr=yr+1900;

   if(Pfss_Form->Stop!=NULL) //aey aaou iino?iaiey ia ?aaiie aaoa caeaca ni STOP
    if(Pfss_Form->Stop->date.y>2000 & (int)Pfss_Form->Stop->date.d != dy)
     { yr=Pfss_Form->Stop->date.y; mn=Pfss_Form->Stop->date.m; dy=Pfss_Form->Stop->date.d;
     }

   s2=IntToStr(yr)+".";
   if(mn<10) s2+="0"; s2+=IntToStr(mn)+".";
   if(dy<10) s2+="0"; s2+=IntToStr(dy);
   title="Created  "+s2;
   title+="   L0: "+IntToStr((int)L0d);


   Plot_2dV(Pfss_Form->Image1,cl_sh,"",nr,np,nt,vrfi,vrq,scale_r,title);



   delete[] t0;
   delete[] t1au;
   delete[] v0;
   delete[] b0;
   delete[] v1au;
   delete[] b1au;
   delete[] v2d;
   delete[] b2d;

   delete[] vrfi;

   delete[] vrq;

   Pfss_Form->Label0->Caption="";

   Pfss_Form->Label0->Update();


}

void Tsyn_map::Wsa_3d(int kl_save,struct Grid3D *grid3d, float *v_s, float *brss_s, double *date,float *speed, int *imf, int tslen,float scale_r,float L0d,int cl_sh)

{ //au?eneaiea nei?inoe n 3d

    int i, j,size,k,k1,key_out=0;
    int np = grid3d->np;
    int nt = grid3d->nt;
    int nr = grid3d->nr;

    double *t0;
    double *t1au;
    float *v0, *b0, *v1au, *b1au;
    float *vrq,*vrfi;
    AnsiString s1;
    FILE *fl_out;
    AnsiString flnm_out;
    float f,rr,al,lng,lat,aa,bb,B0f;
    flnm_out="vv_eclipcic.txt";

    (double*) t0=new double[np+1];
    (double*) t1au=new double[np+1];
    (float*)  v0=new float[np+1];
    (float*)  b0=new float[np+1]; //oaie B0
    (float*)  v1au=new float[np+1];
    (float*)  b1au=new float[np+1];

    (float*)  vrq=new float[(nr+1)*(nt+1)+10];
    (float*)  vrfi=new float[(nr+1)*(np+1)+10];

     float *v2d,*b2d; //ianneau r x np aey ?aeiino?oeoee a ieineinoe
     size=(np+1)*(nr+1)+10;
     (float*)  v2d=new float[size];
     (float*)  b2d=new float[size];


    for (i = 0; i < np; i++)
        t0[i] = grid3d->time[np - 1 - i];  //ia?aai?a?eaaai ii a?aiaie

    int n_ecl;
    B0f=grid3d->b0[0];
    n_ecl=nt/2;
     if(fabs(B0f)<8)
      n_ecl=nt/2-B0f/180*nt;

    // Make date double
//    double date_tmp[tslen];
//    for (i = 0; i < tslen; i++) date_tmp[i] = date[i];
    float *speed_tmp;
    int *imf_tmp;

    if(kl_save==1)
     { flnm_out="vv_3d.txt";
       fl_out=fopen(flnm_out.c_str(), "w");
     }

    if(L0d>360) L0d=L0d-360;
    if(L0d<0)   L0d=L0d+360;


    for (j = 0; j < nt; j++) //oe?ioa
    {  //ia?aai?a?eaaai ii a?aiaie
        for (i = 0; i < np; i++)
         { v0[i] = v_s[j * np + np - 1 - i];
           b0[i] = brss_s[j * np + np - 1 - i];
          }

       lat=180.0*j/nt;
       //i?aia?aciaaiea ia ea?eiaoiiianeie naoea n o?aoii auno?uo iioie
        propagate2d(np,nr,t0, v0, b0, t1au, v1au, b1au,v2d,b2d,scale_r);

        speed_tmp = speed + j * tslen;
        imf_tmp = imf + j * tslen;
      //i?aia?aciaaiea ia ea?eaioiiianeie naoea n o?aoii auno?uo iioie
     //  fixts2d(t1au, v1au, b1au, np, date, speed_tmp, imf_tmp, tslen);
     //caiiieiaai oaee

     lng=360.0*j/nt;

       if(j==n_ecl &  key_out==0) //caiiieiaai e auaiaei ia ye?ai ?ac?ac ii ?aaeono e aieaioa aey L0
        {
        //auaiaei nei?inou a oaee
         int l_shift=-90;
          for(int ii=0;ii<nr;ii++)
            for(int jj=0;jj<np;jj++)
             { float kf=jj+(-L0d+l_shift)/360*np;  //iiaai? ia aieaioo
               if(kf<0) kf=kf+np;
               if(kf>=np) kf=kf-np;
               k=kf;
               int jj2=k+1;
               bb=kf-k; aa=1.0-bb;
               if(k<0)  k=np+k;  if(k<0) k=np+k;  if(k<0) k=np+k;
               if(k>=np) k=k-np; if(k>=np) k=k-np; if(k>=np) k=k-np;
               if(jj2<0) jj2=np+jj2;   if(jj2<0) jj2=np+jj2;   if(jj2<0) jj2=np+jj2;
               if(jj2>=np) jj2=jj2-np; if(jj2>=np) jj2=jj2-np; if(jj2>=np) jj2=jj2-np;
               float f2=*(v2d+jj2*nr+ii);
               f=*(v2d+k*nr+ii)*aa+bb*f2;
               rr=RS+scale_r*ii*PROPDIST / nr;
               al=grid3d->ph[k];
            //   fprintf(fl_out," %9.2f   %9.3f %9.1f \n",rr,al,f);
               *(vrfi+jj*nr+ii)=f;
             }
          key_out=1;
        }//j=L0d

        s1="th: "+IntToStr(j)+" ";
        Pfss_Form->Label0->Caption=s1;
        Pfss_Form->Label0->Update();
        Pfss_Form->Print_Mem(0);
       //?ac?ac ii ?aaeono e oe?ioa
        k=(-L0d)/360*np;
        if(k<0) k=np+k;   if(k<0) k=np+k;
        if(k>=np) k=k-np; if(k>=np) k=k-np;
        for(int ii=0;ii<nr;ii++)
        { int jecl=j+(nt/2-n_ecl);
          if(jecl<0) jecl=0; if(jecl>nt-1) jecl=nt-1;
          *(vrq+jecl*nr+ii)=*(v2d+k*nr+ii);
        }

        //auaiaei nei?inou a oaee
        if(kl_save==1)
          for(int ii=0;ii<nr;ii++)
            for(int jj=0;jj<np;jj++)
             { f=*(v2d+jj*nr+ii);
               rr=RS+scale_r*ii*PROPDIST / nr;
               al=grid3d->ph[jj]*180/Pi;
               fprintf(fl_out," %7.3f %7.2f  %9.3f %9.2f \n",rr,lat,al,f);
             }

    }//j oe?ioa

   if(kl_save==1)
    fclose(fl_out);

   //?ac?ac ii ?aaeono e oe?ioa
   AnsiString title,st2,s2;
   int yr,mn,dy;

   st2=DateToStr(Date());
   int i1=st2.Pos(".");
   if(i1>0) {st2=st2.Delete(i1,1); st2=st2.Insert(" ",i1);}
   i1=st2.Pos(".");
   if(i1>0) {st2=st2.Delete(i1,1); st2=st2.Insert(" ",i1);}
   sscanf(st2.c_str(),"%d %d %d",&dy,&mn,&yr);
   if(dy>31 ) { yr=i1; yr=dy;dy=i1; }
   if(yr<10) yr=yr+2000;
   if(yr<100) yr=yr+1900;

   if(Pfss_Form->Stop!=NULL) //aey aaou iino?iaiey ia ?aaiie aaoa caeaca ni STOP
    if(Pfss_Form->Stop->date.y>2000 & (int)Pfss_Form->Stop->date.d != dy)
     { yr=Pfss_Form->Stop->date.y; mn=Pfss_Form->Stop->date.m; dy=Pfss_Form->Stop->date.d;
     }

   s2=IntToStr(yr)+".";
   if(mn<10) s2+="0"; s2+=IntToStr(mn)+".";
   if(dy<10) s2+="0"; s2+=IntToStr(dy);
   title="Created  "+s2;
   title+="   L0: "+IntToStr((int)L0d);


   Plot_2dV(Pfss_Form->Image1,cl_sh,"",nr,np,nt,vrfi,vrq,scale_r,title);



   delete[] t0;
   delete[] t1au;
   delete[] v0;
   delete[] b0;
   delete[] v1au;
   delete[] b1au;
   delete[] v2d;
   delete[] b2d;

   delete[] vrfi;

   delete[] vrq;

   Pfss_Form->Label0->Caption="";

   Pfss_Form->Label0->Update();


}

 void Tsyn_map::Plot_2dV(TImage *Image1,int cl_sh,AnsiString flnm,int nr,int np,int nt,float *v2d,float *vrq,float scale_r,AnsiString stit)

 {//?enoai nei?inou a ieineinoe yeeeioeee  e ?ac?ac ii ?aaeono e oe?ioa

   int i,j;

   int xc,yc,Rb,cl,x,y,x2,y2;

   float r,rr,al,f,b,mn,mx,mn1,mx1,avr=0,avr2=0,fr1,fr2,fa1,fa2;

   float dr,dal,ar,br,aa,ba;

//   int cl_sh=21;

   int xw1,xw2,yw1,yw2,dxw,dyw;

   AnsiString title;


   if(cl_sh<0 || cl_sh>30)

     cl_sh=27;//ioneiaai e ?aeoiio e e?aniiio


   xc=400;

   yc=300;
   Rb=200;

   xw1=Pfss_Form->Plot_map->Xw1;
   xw2=Pfss_Form->Plot_map->Xw2;
   yw1=Pfss_Form->Plot_map->Yw1;
   yw2=Pfss_Form->Plot_map->Yw2;

   Pfss_Form->Plot_map->Xw1=10;
   Pfss_Form->Plot_map->Yw1=yc-Rb;

   Pfss_Form->Plot_map->Xw2=100+2*xc+Rb*3;
   Pfss_Form->Plot_map->Yw2=yc+Rb;


   Image1->Width=dxw=200+xc+Rb*2;
   Image1->Height=dyw=Rb*2+300;


   Graphics::TBitmap *Btmp;
   Btmp = new Graphics::TBitmap();
   Btmp->Width=dxw;
   Btmp->Height=dyw;
   Btmp->Canvas->Brush->Color = clWhite;
   Btmp->Canvas->Brush->Style = bsSolid;
   Btmp->Canvas->FloodFill(0,0,clWhite, fsBorder);
   Image1->AutoSize=true;
  // Image1->Stretch=true;
   Image1->Picture->Assign(Btmp);
   delete Btmp;


   //Image1->Stretch=true;
   Image1->Canvas->Brush->Color = clBlack;
   Image1->Canvas->Brush->Color = clWhite;

//   TRect NewRect = Rect(0, 0,Pfss_Form->Image1->Width,Pfss_Form->Image1->Height);
//   Image1->Canvas->FillRect(NewRect);


    mn1=1e6; mx1=-1e6; avr=avr2=0;

     for(i=0;i<nr;i++)

      for(j=0;j<np;j++)
       { f=*(v2d+j*nr+i);
         if(f<mn1) mn1=f; if(f>mx1) mx1=f;
         avr+=f;
       }
     avr/=0.001+nr*np;


    mn=300; mx=1500;

    mn=300; mx=800;

    if(mx1>mx)

       {i=mx1/200+1; mx=i*200;}

   // i=mx/100;

   // mx=100.0*(i+0.5);


    dr=(1.0-RS/(scale_r*PROPDIST))/nr;

    dal=2*Pi/np;

    //?acac a ieineinoe yeeeioeee

     for(x=xc-Rb;x<xc+Rb;x++)

        for(y=yc-Rb;y<yc+Rb;y++)

         { XY_Polar(x,y,xc,yc,Rb,&al,&r);

           if(r>1)

             continue;

           if(r<RS/(scale_r*PROPDIST))

             continue;

           rr=r*PROPDIST;

           i=(r-RS/(scale_r*PROPDIST))/dr;

           j=al*np/(2*Pi);

           ar=(r-i*dr)/dr; br=1.0-ar;

           aa=(al-j*dal)/dal; ba=1.0-aa;

           int j1=j+1; if(j1>=np) j1=0;

           fa1=*(v2d+j*nr+i)*ba+*(v2d+j1*nr+i)*aa;

           int i1=i+1; if(i1>=nr) i1=nr-1;

           fa2=*(v2d+j*nr+i1)*ba+*(v2d+j1*nr+i1)*aa;

           f=fa1*br+fa2*ar;


           cl=Color_MBall(cl_sh,f,mn,mx,avr,avr2);

           Image1->Canvas->Pixels[x][y]=cl;

         }


      x=Pfss_Form->Plot_map->Xw2;

      x2=Pfss_Form->Plot_map->Xw2=-50;

      //Pfss_Form->Canvas->Font->Color=clWhite;

      Pfss_Form->Plot_map->Draw_Scale(Image1,cl_sh,mn,mx,avr,0);

      Pfss_Form->Plot_map->Xw2=x;

   //iaaienu

   y2=Pfss_Form->Plot_map->Yw1-30;

   Image1->Canvas->Font->Name="Arial";

   Image1->Canvas->Font->Size=12;
   Image1->Canvas->TextOut(x2,y2,"                                                                              ");
   title="Radial velocity (km/s)";

   Image1->Canvas->Font->Size=12;
   Image1->Canvas->TextOut(x2+60,y2,title);


  //?enoai Nieioa

   Image1->Canvas->Pen->Width=2;

   Image1->Canvas->Pen->Color=clBlack;

   Image1->Canvas->Pen->Mode=pmCopy;

   Image1->Canvas->Brush->Style=bsClear;

   Image1->Canvas->Ellipse(xc-7,yc-7,xc+7,yc+7);

   Image1->Canvas->Brush->Color = clYellow;

   Image1->Canvas->Brush->Style = bsSolid;

   Image1->Canvas->Ellipse(xc-5,yc-5,xc+5,yc+5);

   Image1->Canvas->Pen->Width=1;

   Image1->Canvas->Brush->Style=bsClear;



//?enoai Caie?

  if(scale_r>1.0)

   { x=xc+Rb/scale_r; y=yc;

     Image1->Canvas->Brush->Style=bsClear;

     Image1->Canvas->Pen->Width=1;

     Image1->Canvas->Pen->Color=clBlack;

     int dx=(float)Rb/scale_r;

     Image1->Canvas->Ellipse(xc-dx,yc-dx,xc+dx,yc+dx);


     Image1->Canvas->Pen->Width=2;

     Image1->Canvas->Pen->Color=clBlack;

     Image1->Canvas->Pen->Mode=pmCopy;

     Image1->Canvas->Brush->Style=bsClear;

     Image1->Canvas->Ellipse(x-5,y-5,x+5,y+5);

     Image1->Canvas->Brush->Color = clLime;

     Image1->Canvas->Brush->Style = bsSolid;

     Image1->Canvas->Ellipse(x-4,y-4,x+4,y+4);

   }


    //?ac?ac ii oe?ioa nt

    float angl=Pi/6;

    xc=xc+Rb+100;

    dal=Pi/nt;

     for(x=xc;x<xc+Rb;x++)

      for(y=yc-Rb;y<yc+Rb;y++)

        { XY_Polar(x,y,xc,yc,Rb,&al,&r);

           if(r>1)

             continue;

           if(r<RS/(scale_r*PROPDIST))

             continue;

           if(!(al>angl & al<Pi-angl))

            continue;


           rr=r*PROPDIST;

           i=(r-RS/(scale_r*PROPDIST))/dr;

           j=al*nt/(Pi);

           ar=(r-i*dr)/dr; br=1.0-ar;

           aa=(al-j*dal)/dal; ba=1.0-aa;

           int j1=j+1; if(j1>=nt) j1=0;

           fa1=*(vrq+j*nr+i)*ba+*(vrq+j1*nr+i)*aa;

           int i1=i+1; if(i1>=nr) i1=nr-1;

           fa2=*(vrq+j*nr+i1)*ba+*(vrq+j1*nr+i1)*aa;

           f=fa1*br+fa2*ar;


           cl=Color_MBall(cl_sh,f,mn,mx,avr,avr2);

           Image1->Canvas->Pixels[x][y]=cl;

         }

  //?enoai Nieioa

   Image1->Canvas->Pen->Width=2;

   Image1->Canvas->Pen->Color=clBlack;

   Image1->Canvas->Pen->Mode=pmCopy;

   Image1->Canvas->Brush->Style=bsClear;

   Image1->Canvas->Ellipse(xc-7,yc-7,xc+7,yc+7);

   Image1->Canvas->Brush->Color = clYellow;

   Image1->Canvas->Brush->Style = bsSolid;

   Image1->Canvas->Ellipse(xc-5,yc-5,xc+5,yc+5);


   //?enoai Caie?

  if(scale_r>1.0)

   { float dr=Rb/scale_r;

     Image1->Canvas->Brush->Style=bsClear;

     Image1->Canvas->Pen->Width=1;

     Image1->Canvas->Pen->Color=clBlack;

     int x1,x2,x3,x4,y1,y2,y3,y4,dx=(float)Rb/scale_r;

     x1=xc-dr; y1=yc-dr;

     x2=xc+dr; y2=yc+dr;

     x3=xc+sin(angl)*dr; y3=yc-cos(angl)*dr;

     x4=xc+sin(angl)*dr; y4=yc+cos(angl)*dr;

     Image1->Canvas->Arc(x1,y1,x2,y2,x4,y4,x3,y3);

     x=xc+Rb/scale_r; y=yc;

     Image1->Canvas->Pen->Width=2;

     Image1->Canvas->Pen->Color=clBlack;

     Image1->Canvas->Pen->Mode=pmCopy;

     Image1->Canvas->Brush->Style=bsClear;

     Image1->Canvas->Ellipse(x-5,y-5,x+5,y+5);

     Image1->Canvas->Brush->Color = clLime;

     Image1->Canvas->Brush->Style = bsSolid;

     Image1->Canvas->Ellipse(x-4,y-4,x+4,y+4);

   }


   Pfss_Form->Plot_map->Xw1=xw1;

   Pfss_Form->Plot_map->Xw2=xw2;
   Pfss_Form->Plot_map->Yw1=yw1;
   Pfss_Form->Plot_map->Yw2=yw2;



   Image1->Canvas->Pen->Width=1;

   Image1->Canvas->Brush->Style=bsClear;


  Image1->Canvas->Font->Size=16;

  Image1->Canvas->TextOut(dxw/2-100,20,"Kislovodsk Mountain Astronomical Station");
  Image1->Canvas->Font->Size=10;
  Image1->Canvas->TextOut(10,dyw-20,stit);


   Image1->Update();


 }


TColor Color_MBall(int kl_sch,float h,float mn,float mx,float avr,float avr2)
{//oaaoa ieenaeae
 int rc,gc,bc,c,sgn=1;
  DWORD  L;
  TColor cl;
  float f,kf=1,fval,fmedian,fmedian2;
  int bt,bt1,kp;

//  int ci[]={ 0, 20, 40,  60, 80, 100, 115, 130, 145, 160, 170, 180, 200};
  int ci[]={ 5, 20, 35,  55, 75, 85, 100, 120, 140, 160, 180, 200,220,230};

  int vc[]={-512,-256,-128,-64, -32, -16,  -8,  -4,  -2,  -1,   0, 1,2,4,8,16,32,64,128,256,512};
  int kv=21,ik=0;

 kf=1;
 if(avr2>1e-4)
   kf=255/(avr2)*1.5;
  rc=bc=gc=0;
  if(h<0) sgn=-1;

  if(kl_sch==17)
   for(int i=0;i<21;i++)
    vc[i]=0.5*vc[i];

  if(kl_sch==18)
   {for(int i=0;i<21;i++)
      vc[i]=0.2*vc[i];
     kl_sch=17;
   }


  switch (kl_sch)
    { case 1:
      rc=bc=gc=0;
      if(h<0 & mn<0)
       { c=fabs(h)*kf; //((avr2+1)*10);
        c=min((float)c,(float)254); c=max((float)10,(float)c); bc=c;
        }

      if(h>0 & mx>0)
             { c=fabs(h)*kf; ///((avr2+1)*10);
               c=min((float)c,(float)254); c=max((float)10,(float)c); rc=c;
              }

            gc=c;
            break;
           case  2:
             rc=bc=gc=0;
              if(h<0){ if(fabs(h)>32000) c=0;
                   else c=255-255.0*(h*2.5-mn)/(0-mn);
                   c=min(255,c); c=max(0,c);
                   bc=c; gc=c/2;
                 }
             else { if(fabs(h)>32000) c=0;
                    else   c=255.0*(h*2.5-0)/(mx-0);
                    c=min(255,c); c=max(0,c);
                    rc=c; gc=c/2;
                  }

            break;
           case  3:
            rc=bc=gc=0;
            if(sgn>0)
            {  c=50+50*(h-avr)/(avr2);
               c=min(255,c); c=max(0,c);
               rc=c;
            }
            if(sgn<0)
            { c=50+50*fabs(h-avr)/(avr2);
              c=min(255,c); c=max(0,c);
              bc=c;
            }
            break;
          case 4:
           if(sgn>0)
            { if(avr2>0)
              c=125+125.0*(h-avr)/(2*avr2);
             c=min(255,c); c=max(0,c);
             gc=rc=bc=c;
             rc=bc=gc=0;
             c=255.0*(h-mn)/(mx-mn);
             c=min(255,c); c=max(0,c);
             rc=c; gc=c/2; bc=0;
             }
           else
            {if(avr2>0)
              c=125+125.0*(h-avr)/(2*avr2);
             c=min(255,c); c=max(0,c);
             gc=rc=bc=c;
             rc=bc=gc=0;
             c=255.0*(h-mn)/(mx-mn);
             c=min(255,c); c=max(0,c);
             gc=c; rc=c/2; bc=0;
            }
           break;
           case  5:
             rc=bc=gc=0;
             c=255.0*(h-mn)/(mx-mn);
             c=min(255,c); c=max(0,c);
             gc=rc=bc=c;

            break;
           case  6: //grayscale
             rc=bc=gc=0;
             if(avr2>0)
              c=125+125.0*(h-avr)/(2*avr2);
             c=min(255,c); c=max(0,c);
             gc=rc=bc=c;

            break;
           case  7:
             rc=bc=gc=0;
             if(avr2>0)
              c=125+125.0*(h-avr)/(2*avr2);
             c=min(255,c); c=max(0,c);
             gc=rc=c; bc=0;
            break;
           case  8:
             rc=bc=gc=0;
             if(avr2>0)
              c=125+125.0*(h-avr)/(3*avr2);
             c=min(255,c); c=max(0,c);
             gc=bc=c; rc=0;
            break;
           case  9:
            rc=bc=gc=0;
            if(sgn>0)
            {  c=50+20*(h-avr)/(avr2);
               c=min(255,c); c=max(0,c);
               rc=c;
            }
            if(sgn<0)
            { c=50+20*fabs(h-avr)/(avr2);
              c=min(255,c); c=max(0,c);
              bc=c;
            }
            break;
           case  10:
             rc=bc=gc=0;
             if(avr2>0)
              c=125+125.0*(h-avr)/(3*avr2);
             c=min(255,c); c=max(0,c);
             gc=c; bc=c/2; rc=0;
            break;
           case  11:
             rc=bc=gc=0;
             if(avr2>0)
              c=125+125.0*(h-avr)/(3*avr2);
             c=min(255,c); c=max(0,c);
             gc=c; bc=c/2; rc=255;
            break;
           case  12:
             rc=bc=gc=0;
             if(avr2>0)
              c=125+125.0*(h-avr)/(3*avr2);
             c=min(255,c); c=max(0,c);
             gc=c/2; bc=255; rc=c;
            break;
           case  13:
             rc=bc=gc=0;
             if(avr2>0)
              c=125+125.0*(h-avr)/(3*avr2);
             c=min(255,c); c=max(0,c);
             gc=255; bc=c; rc=c/4;
            break;
           case  14:  //aaa iooaiea na?iai
             if(h>=0) cl=clLtGray;
             else cl=clGray;
            return cl;
       case  16:
             rc=bc=gc=0;
             if(avr2>0)
              c=125+125.0*(h-avr)/(3*avr2);
             c=min(255,c); c=max(0,c);
             gc=c; bc=255; rc=200;
            break;
        case  17: //na?ia
          fval=fabs(h);
          ik=0;
           for(kp=0;kp<kv-1;kp++)
             {  if(h>=vc[kp] & h<vc[kp+1])
                 { ik=kp+1;
                   break;
                 }
             }
           if(ik>kv/2)
               bt1=ci[kv-ik];
           else
             bt1=ci[ik];

         rc=bc=gc=0;
          if(h<0)  {bc=255; bc=rc=gc=255-bt1;}
          if(h>0) { rc=255; rc=bc=gc=bt1;}

          if(fabs(h)<0.1)  rc=bc=gc=220;
           break;
       case 15: //iaaieoiia iiea
          fval=fabs(h);
          ik=0;
           for(kp=0;kp<kv-1;kp++)
             {  if(h>=vc[kp] & h<vc[kp+1])
                 { ik=kp+1;
                   break;
                 }
             }
           if(ik>kv/2)
               bt1=ci[kv-ik];
           else
             bt1=ci[ik];

         rc=bc=gc=0;
          if(h<0)  {bc=255; rc=gc=bt1;}
          if(h>0) { rc=255; bc=gc=bt1;}
          if(fabs(h)<0.1)  rc=bc=gc=240;
          break;
       case 20: //io neiaai e e?aniiio

       if( h<mn)
         h=mn;//  { bc=255; rc=gc=0;}
       if(h>=mx)
        h=mx;//  { rc=gc=255; bc=0; } //?aeoue
        fval=(h-mn)/(mx-mn);
        gc=fabs(sin(fval*2*Pi/2))*255;
        bc=(1.0-fval)*255;
        rc=fval*255;
        break;
       case 21: //io neiaai e ?aeoiio
       if( h<mn)
        h=mn; // { bc=255; rc=gc=0;}
       if(h>=mx)
          h=mx; //rc=gc=255; bc=0; } //?aeoue

         fval=(h-mn)/(mx-mn);
          //gc=rc=fabs(sin(fval*2*Pi/2))*255;
          bc=(1.0-fval)*255;
          rc=gc=fval*255;
        break;
       case 22: //io neiaai e ?aeoiio
       if( h<mn)
        h=mn; // { bc=255; rc=gc=0;}
       if(h>=mx)
          h=mx; //rc=gc=255; bc=0; } //?aeoue

         fval=(h-mn)/(mx-mn);
          //gc=rc=fabs(sin(fval*2*Pi/2))*255;
          rc=(1.0-fval)*255;
          bc=gc=fval*255;
         break;
     case 23: // neiaa e e?aniia
       rc=bc=gc=0;
       if(h>=0)
         rc=255;
        else
         bc=255;
        break;
    case 24: //io neiaai e ?aeoiio (na?aaeia) e e?aniiio (iaeneioi)
       fmedian=(mx+mn)/2;
       if( h<mn)
        h=mn;
       if(h>=mx)
          h=mx; //rc=gc=255; bc=0; } //?aeoue

        if(h<fmedian)//neia ?aeoia
         {fval=(h-mn)/(fmedian-mn);
          bc=(1.0-fval)*255;
          rc=fval*255;
          gc=fval*255;
         }
        if(h>=fmedian)
         { bc=0;
           fval=(h-fmedian)/(mx-fmedian);
           rc=(1.0-fval)*255;
           gc=(1.0-fval)*255;
          // bc=gc;
          }
        break;
    case 25: //io neiaai e ?aeoiio (na?aaeia) e e?aniiio (iaeneioi)
       fmedian=(mx+mn)/2;
       if( h<mn)
        h=mn;
       if(h>=mx)
          h=mx; //rc=gc=255; bc=0; } //?aeoue

        if(h<fmedian)//neia ?aeoia
         {fval=(h-mn)/(fmedian-mn);
          rc=(1-fval)*255;
          bc=fval*255;
          gc=(1.0-fval)*255;
         }
        if(h>=fmedian)
         { gc=0;
           fval=(h-fmedian)/(mx-fmedian);
           rc=fval*225;
           bc=(1.0-fval)*255;
          }
        break;
    case 26: //io neiaai e ?aeoiio (na?aaeia) e e?aniiio (iaeneioi)
       fmedian=(mx+mn)/2;
       if( h<mn)
        h=mn;
       if(h>=mx)
          h=mx; //rc=gc=255; bc=0; } //?aeoue

        if(h<fmedian)//neia ?aeoia
         {fval=(h-mn)/(fmedian-mn);
          bc=(1.0-fval)*255;
          rc=fval*120;
          gc=fval*255;
         }
        if(h>=fmedian)
         { bc=0;
           fval=(h-fmedian)/(mx-fmedian);
           rc=120+fval*125;
           gc=(1.0-fval)*255;
          // bc=gc;
          }
        break;
    case 27: //io neiaai e ?aeoiio (1/3) e e?aniiio (2/3) e ?a?iiio(iaeneioi)
       fmedian=(mx+mn)/3;
       fmedian2=(mx+mn)*0.75;
       if( h<mn)
        h=mn;
       if(h>=mx)
          h=mx; //rc=gc=255; bc=0; } //?aeoue

        if(h<fmedian)//neia ?aeoia
         {fval=(h-mn)/(fmedian-mn);
          bc=(1.0-fval)*255;
          rc=gc=fval*255;
         }

        if(h>=fmedian & h<fmedian2)
         { bc=0;
           fval=(h-fmedian)/(fmedian2-fmedian);
           rc=255;
           gc=(1-fval)*255;
           bc=fval*255;
          // bc=gc;
          }
        if(h>=fmedian2)
         { fval=(h-fmedian2)/(mx-fmedian2);
           bc=(1.0-fval)*255;
           rc=(1.0-fval)*255;
           gc=(1.0-fval)*fval*255;
          // bc=gc;
          }
        break;
    case 28: //io neiaai e ?aeoiio (1/3) e e?aniiio (2/3) e ?a?iiio(iaeneioi)
       fmedian=(mx+mn)/2;
       fmedian2=(mx+mn)*0.75;
       if( h<mn)
        h=mn;
       if(h>=mx)
          h=mx; //rc=gc=255; bc=0; }

        if(h<fmedian)//neia->?aeoia
         {fval=(h-mn)/(fmedian-mn);
          bc=(1.0-fval)*255;
          rc=gc=fval*255; //?aeoue
         }

        if(h>=fmedian & h<fmedian2)  //?aeoue-e?aniue
         { bc=0;
           fval=(h-fmedian)/(fmedian2-fmedian);
           rc=255;
           gc=(1-fval)*255;
           bc=0;
          // bc=gc;
          }
        if(h>=fmedian2) //e?aniue-?a?iue
         { fval=(h-fmedian2)/(mx-fmedian2);
           bc=0;
           rc=(1.0-fval)*255;
           gc=0;
          // bc=gc;
          }
        break;
    case 29: //io neiaai e ?aeoiio (1/3) e e?aniiio (2/3) e ?a?iiio(iaeneioi)
       fmedian=(mx+mn)/3;
       fmedian2=(mx+mn)*0.7;
       if( h<mn)
        h=mn;
       if(h>=mx)
          h=mx; //rc=gc=255; bc=0; }

        if(h<fmedian)//neia->?aeoia
         {fval=(h-mn)/(fmedian-mn);
          gc=(1.0-fval)*255;
          rc=bc=fval*255; //?aeoue
         }

        if(h>=fmedian & h<fmedian2)  //?aeoue-e?aniue
         { bc=0;
           fval=(h-fmedian)/(fmedian2-fmedian);
           rc=255;
           bc=(1-fval)*255;
           gc=0;
          // bc=gc;
          }
        if(h>=fmedian2) //e?aniue-?a?iue
         { fval=(h-fmedian2)/(mx-fmedian2);
           gc=0;
           rc=(1.0-fval)*255;
           bc=0;
          // bc=gc;
          }
        break;
    case 30: //io neiaai e ?aeoiio (1/3) e e?aniiio (2/3) e ?a?iiio(iaeneioi)
       fmedian=(mx+mn)/3;
       fmedian2=(mx+mn)*0.66;
       if( h<mn)
        h=mn;
       if(h>=mx)
          h=mx; //rc=gc=255; bc=0; }

        if(h<fmedian)//neia->?aeoia
         {fval=(h-mn)/(fmedian-mn);
          rc=(1.0-fval)*255;
          bc=gc=fval*255; //?aeoue
         }

        if(h>=fmedian & h<fmedian2)  //?aeoue-e?aniue
         { bc=0;
           fval=(h-fmedian)/(fmedian2-fmedian);
           bc=255;
           gc=(1-fval)*255;
           rc=0;
          // bc=gc;
          }
        if(h>=fmedian2) //e?aniue-?a?iue
         { fval=(h-fmedian2)/(mx-fmedian2);
           rc=0;
           bc=(1.0-fval)*255;
           gc=0;
          // bc=gc;
          }
        break;
      default:
           if(sgn>0) cl=clWhite;
           else cl=clLtGray;
           return cl;
        }

       L=rc | 0x00000000;
       L=(gc << 8) | L;
       L=(bc << 16) | L;
       cl=TColor(L);

   return cl;
}




