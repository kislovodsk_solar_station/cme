#-------------------------------------------------
#
# Project created by QtCreator 2017-11-27T10:52:29
#
#-------------------------------------------------
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11
TARGET = Drag-based-model
TEMPLATE = app


SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/diffequation.cpp \
    src/solarwind.cpp \
    src/cmeImpl.cpp \
    src/cme.cpp \
    src/dbmodelImpl.cpp \
    src/cme_parameters.cpp \
    src/dbmodel.cpp

HEADERS  += src/mainwindow.h \
    src/diffequation.h \
    src/solarwind.h \
    src/cmeImpl.h \
    src/cme.h \
    src/dbmodelImpl.h \
    src/cme_parameters.h \
    src/dbmodel.h

FORMS    += ui/mainwindow.ui

win32: RC_ICONS = res/game.ico
