#ifndef CME_PARAMETERS_H
#define CME_PARAMETERS_H

#include <vector>
#include <QDebug>
#include "cme.h"

const unsigned current_points_number = 1;
const double sun_radius = 692000;
const double current_init_distance = 20;
const double AE = 1.5e8;
const double day_seconds = 24 * 3600;
const double PI = 3.1415926535;
const double degrees_to_rad = PI / 180;

inline double speed_sin_distrib(double alpha)
{
    double res = cos(alpha);
//    qDebug() << "speed distr: alpha = "<< alpha <<", sin = "<< res;
    return res;
}

inline double speed_line_distrib(double alpha, double cone)
{
    double res = 1 - fabs(alpha) / cone;
    qDebug() << "speed distr: alpha = "<< alpha <<", return = "<< res;
    return res;
}


// Struct with motion parameters for points
struct CME_parameters
{
    CME_parameters(const CME &cme, unsigned points_number = 1);
    CME_parameters(unsigned points_number = 1);
    CME_parameters(const CME_parameters &other);
    CME_parameters & operator=(const CME_parameters &other);
    ~CME_parameters();

    std::vector<double> *distance;
    std::vector<double> *phi;
    std::vector<double> *radial_speed;
    std::vector<double> *phi_speed;
    std::vector<double> *radial_acceleration;
    std::vector<double> *phi_acceleration;

private:
    unsigned n;
    void swap(CME_parameters & other);
    void init_cone(const CME & cme, unsigned n);
    void init_old(CME *cme, unsigned n);
public:
    inline unsigned points_number() const { return n; }
};


#endif // CME_PARAMETERS_H
