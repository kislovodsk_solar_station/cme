//----------------------------------------------------------------
// Drag-based model-----------------------------------------------
//----------------------------------------------------------------

#include "dbmodelImpl.h"

DBModelImpl::DBModelImpl(QObject *parent, CME *cme, SolarWind *sw,
                 unsigned points_number_, double time_step_) :
    QObject(parent),
    traectory(0),
    cme(cme),
    sw(sw),
    points_number(points_number_),
    init_gamma(1e-7),
    time_step(time_step_)
{ 
    qDebug() << "DBModel: object was created!";
    //    qDebug() << parameters_in_time.pointer;
}

DBModelImpl::~DBModelImpl()
{
    if (traectory != 0)
        delete traectory;
}

void DBModelImpl::setPointsNumber(unsigned N) { points_number = N; }
void DBModelImpl::setTimeStep(double dt)      { time_step = dt; }

unsigned DBModelImpl::getPointsNumber() const { return points_number; }
double   DBModelImpl::getTimeStep()     const { return time_step; }
double   DBModelImpl::getInitGamma()    const { return init_gamma; }

// Calculate points traectories
std::vector<CME_parameters> *DBModelImpl::calculate(const CME &cme)
{
    // create start parameters
    CME_parameters p(cme, points_number);

    if (traectory == 0)
        traectory = new std::vector<CME_parameters>;
    else
        traectory->clear();

    //    traectory->reserve(number_of_steps);
    traectory->push_back(p);

    // call Runge-Cutt method
    runge_cutt(*traectory);
    qDebug() << "calculate finished!";
    return traectory;

}

void DBModelImpl::runge_cutt(std::vector<CME_parameters> &traectory)
{
    qDebug() << "runge-cutt called!";

    unsigned n = traectory.back().points_number();
    qDebug() << n/2;
    unsigned j = 0;
//    for (long j = 1; j != 1000; ++j)
    while (traectory.back().distance->at(n/2) < 269 * sun_radius)
    {
//        qDebug() << "j =" << j;
         traectory.push_back(runge_cutt_step(traectory.back()));
         ++j;
//        double t = time_step * day_seconds * j;
    }
    qDebug() << "runge-cutt exit: j = " << j;
}

CME_parameters DBModelImpl::runge_cutt_step(const CME_parameters & p)
{
//    qDebug() << "runge-cutt step called!";
    CME_parameters result(p);

    double h = time_step * day_seconds;
//    double h = 100;
    double k1, k2, k3, k4;
    double m1, m2, m3, m4;
    double l1, l2, l3, l4;
    double n1, n2, n3, n4;
    double dk, dm, dl, dn;

    try {
    for (unsigned i = 0; i != p.points_number(); i++) {

        k1 = h * p.radial_speed->at(i);
        m1 = h * g1(p.distance->at(i), p.radial_speed->at(i), p.phi->at(i), p.phi_speed->at(i));
        l1 = h * p.phi_speed->at(i);
        n1 = h * g2(p.distance->at(i), p.radial_speed->at(i), p.phi->at(i), p.phi_speed->at(i));
//        qDebug() << k1 << m1 << l1 << n1;

        k2 = h * (p.radial_speed->at(i) + m1/2);
        m2 = h * g1(p.distance->at(i) + k1/2, p.radial_speed->at(i) + m1/2, p.phi->at(i) + l1/2, p.phi_speed->at(i) + n1/2);
        l2 = h * (p.phi_speed->at(i) + n1/2.0);
        n2 = h * g2(p.distance->at(i) + k1/2, p.radial_speed->at(i) + m1/2, p.phi->at(i) + l1/2, p.phi_speed->at(i) + n1/2);
//        qDebug() << k2 << m2 << l2 << n2;
        k3 = h * (p.radial_speed->at(i) + m2/2);
        m3 = h * g1(p.distance->at(i) + k2/2, p.radial_speed->at(i) + m2/2, p.phi->at(i) + l2/2, p.phi_speed->at(i) + n2/2);
        l3 = h * (p.phi_speed->at(i) + n2/2);
        n3 = h * g2(p.distance->at(i) + k2/2, p.radial_speed->at(i) + m2/2, p.phi->at(i) + l2/2, p.phi_speed->at(i) + n2/2);
//        qDebug() << k3 << m3 << l3 << n3;
        k4 = h * (p.radial_speed->at(i) + m3);
        m4 = h * g1(p.distance->at(i) + k3, p.radial_speed->at(i) + m3, p.phi->at(i) + l3, p.phi_speed->at(i) + n3);
        l4 = h * (p.phi_speed->at(i) + n3);
        n4 = h * g2(p.distance->at(i) + k3,  p.radial_speed->at(i) + m3, p.phi->at(i) + l3, p.phi_speed->at(i) + n3);
//        qDebug() << k4 << m4 << l4 << n4;
        dk = (k1 + 2*k2 + 2*k3 + k4) / 6;
        dm = (m1 + 2*m2 + 2*m3 + m4) / 6;
        dl = (l1 + 2*l2 + 2*l3 + l4) / 6;
        dn = (n1 + 2*n2 + 2*n3 + n4) / 6;

//        qDebug() <<"i =" << i << "dk, dm, dl, dn = " <<dk<<dm<< dl << dn;
//        qDebug() << result.distance->at(0) << result.radial_speed->at(0) << result.phi->at(0) << result.phi_speed->at(0);
//        qDebug() << result.phi->at(0) << result.phi_speed->at(0);

        result.distance->at(i) += dk;
        result.radial_speed->at(i) += dm;
        result.phi->at(i) += dl;
        result.phi_speed->at(i) += dn;
    }
//    qDebug() << result.distance->at(0) << result.radial_speed->at(0) << result.phi->at(0) << result.phi_speed->at(0);
    }
    catch(std::out_of_range) {
            qDebug() << "Out of range!";
            result = p;
    }
    return result;
}

double DBModelImpl::gamma(double r, double phi)
{
    return init_gamma;
}

double DBModelImpl::g1(const double &r, const double &vr, const double &phi, const double &vphi)
{
    double temp = vr - sw->value(r, phi);
    return r * vphi * vphi - gamma(r, phi) * temp * fabs(temp);
}

double DBModelImpl::g2(const double &r, const double &vr, const double &phi, const double &vphi)
{
    return -vphi *(gamma(r, phi) * fabs(vphi) + 2*vr) / r;
}
