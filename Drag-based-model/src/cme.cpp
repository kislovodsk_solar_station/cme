#include "cme.h"
#include "cmeImpl.h"
#include <QDebug>

CME::CME(double longitude_, double cone_angle_,
           double size_, double init_distance_,
           double init_speed_)
    : cme(new CMEimpl(longitude_, cone_angle_, size_, init_distance_, init_speed_))
{
    qDebug() << "CME creator!";
}

CME::~CME() { delete cme; }
