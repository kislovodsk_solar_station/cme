#include "dbmodel.h"

DBModel::DBModel(QObject *parent, CME *cme, SolarWind *sw,
                 unsigned points_number_, double time_step_)
    : dbm(new DBModelImpl(parent, cme, sw,
                          points_number_, time_step_))
{
    qDebug() << "DBModel object created!";
}

DBModel::~DBModel()  { delete dbm; }

void DBModel::setPointsNumber(unsigned N) { dbm->setPointsNumber(N); }
void DBModel::setTimeStep(double dt)      { dbm->setTimeStep(dt); }

unsigned DBModel::getPointsNumber()   const { return dbm->getPointsNumber(); }
double   DBModel::getTimeStep()       const { return dbm->getTimeStep(); }
double   DBModel::getInitGamma()      const { return dbm->getInitGamma(); }

std::vector<CME_parameters> *DBModel::calculate(const CME &cme)
{
    return dbm->calculate(cme);
}



