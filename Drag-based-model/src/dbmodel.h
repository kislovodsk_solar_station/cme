//--------------------- Interface for DBModel class ---------------------
//-----------------------------------------------------------------------
#ifndef DBMODEL_H
#define DBMODEL_H

#include <vector>
#include "dbmodelImpl.h"

class DBModel
{
public:
    explicit DBModel(QObject *parent = 0, CME *cme = 0, SolarWind *sw = 0,
            unsigned points_number_ = 1, double time_step_ = 0.001);
    ~DBModel();

    void setPointsNumber(unsigned N);
    void setTimeStep(double dt);

    unsigned getPointsNumber() const;
    double getTimeStep()       const;
    double getInitGamma()      const;

    std::vector<CME_parameters> *calculate(const CME &cme);

private:
    DBModelImpl *dbm;
};

#endif // DBMODEL_H
