#ifndef DIFFEQUATION_H
#define DIFFEQUATION_H

#include <vector>
#include "solarwind.h"
#include "cme_parameters.h"

class DBModelImpl;
class DiffEquation
{
public:
    DBModelImpl *dbm;
    DiffEquation(double r_max_);
    double r_max;
    CME_parameters *p;
    CME_parameters *p_next;

    void calculate(std::vector<CME_parameters> *p);

private:
    double fun1(CME_parameters *p);
    double fun2(CME_parameters *p);

};

#endif // DIFFEQUATION_H
