#ifndef SOLARWIND_H
#define SOLARWIND_H


class SolarWind
{
public:
    SolarWind(double speed_ = 0);
    double value(const double &r, const double &phi) const;
    void setSpeed(double value);

private:
    double speed;
};

#endif // SOLARWIND_H
