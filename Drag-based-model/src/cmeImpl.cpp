#include "cmeImpl.h"

CMEimpl::CMEimpl(double longitude_, double cone_angle_, double size_, double init_distance_, double init_speed_) :
    longitude(longitude_),
    cone_angle(cone_angle_),
    size(size_),
    init_distance(init_distance_),
    init_speed(init_speed_)
{}

void CMEimpl::setLongitude(double phi_)  { longitude = phi_; }
void CMEimpl::setConeAngle(double alpha) { cone_angle = alpha; }
void CMEimpl::setSize(double cme_size_)  { size = cme_size_; }
void CMEimpl::setInitDistance(double R)  { init_distance = R; }
void CMEimpl::setInitSpeed(double V)     { init_speed = V; }

double CMEimpl::getLongitude()    const { return longitude; }
double CMEimpl::getConeAngle()    const { return cone_angle; }
double CMEimpl::getSize()         const { return size; }
double CMEimpl::getInitDistance() const { return init_distance; }
double CMEimpl::getInitSpeed()    const { return init_speed; }
