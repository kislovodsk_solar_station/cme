//--------------------------------------------------------------
//------- Class for representation Coronal Mass Ejection--------
//------- Author: Mikhail Pashchenko, GAS GAO RAN---------------

#ifndef CME_H
#define CME_H

class CMEimpl
{
public:
    explicit CMEimpl(double longitude_ = 0, double cone_angle_ = 0, double size_ = 0,
                 double init_distance_ = 20, double init_speed_ = 0);

    void setLongitude(double phi_);
    void setConeAngle(double alpha);
    void setSize(double cme_size_);
    void setInitDistance(double R);
    void setInitSpeed(double V);

    double getLongitude()    const;
    double getConeAngle()    const;
    double getSize()         const;
    double getInitDistance() const;
    double getInitSpeed()    const;

private:
    double longitude;       // [degrees]
    double cone_angle;      // [degrees]
    double size;            // [R_sun]
    double init_distance;   // [R_sun]
    double init_speed;      // [km/s]
};

#endif // CME_H
