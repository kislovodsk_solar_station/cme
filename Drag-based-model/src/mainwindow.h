#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QMessageBox>
#include <QThread>
#include <QPainter>
#include <QPixmap>
#include <QPointF>
#include <QEvent>
#include <QMouseEvent>
#include <QDir>

#include "dbmodelImpl.h"
//#include "cme.h"
#include "diffequation.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_longitude_doubleSpinBox_valueChanged(double phi);
    void on_initDistance_SpinBox_valueChanged(double r0);
    void on_initAngle_SpinBox_valueChanged(double alpha);
    void on_initSpeed_SpinBox_valueChanged(double v0);
    void on_initSizeSpinBox_valueChanged(double s0);
    void on_pointsNumber_spinBox_valueChanged(int N);
    void on_TimeStep_SpinBox_valueChanged(double dt);
    void on_WindSpeed_spinBox_valueChanged(int speed);

    void on_redSlider_valueChanged(int value);
    void on_greenSlider_valueChanged(int value);
    void on_blueSlider_valueChanged(int value);

    void mousePressEvent(QMouseEvent *event);
//    void paintEvent(QPaintEvent *e);
    void showWarningMessage(const QString &str);

    void on_calculate_button_clicked();
    void paintTraectory(const std::vector<CME_parameters> &tr);
    std::vector<QPointF> polarToWidgetCoordinates(const CME_parameters &p);

private:
    Ui::MainWindow *ui;
    CME *cme;
    double wind_speed;
    SolarWind *sw;
    DBModelImpl *dbm;
    QThread *thread_dbm;
    QPainter *paint;
    QColor color;
    QBrush brush;
    QPixmap* pixmap;
    QPoint position;
    QString path;
    bool save;
    long k;

    uint r;
    uint g;
    uint b;
    int factory;

    // size in pixels
    QPointF sun_center;
    double sunRadius;
};

#endif // MAINWINDOW_H
