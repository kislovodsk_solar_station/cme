#include <QFileDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cme.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    thread_dbm(new QThread),
    sunRadius(153.0 / 216.0),
    save(false),
    factory(1),
    k(0)
{
    ui->setupUi(this);
    cme = new CME(ui->longitude_doubleSpinBox->value(),
                  ui->initAngle_SpinBox->value(),
                  ui->initSizeSpinBox->value(),
                  ui->initDistance_SpinBox->value(),
                  ui->initSpeed_SpinBox->value());
    sw = new SolarWind(ui->WindSpeed_spinBox->value());
    dbm = new DBModelImpl(nullptr, cme, sw, ui->pointsNumber_spinBox->value(), ui->TimeStep_SpinBox->value());
    dbm->moveToThread(thread_dbm);
    thread_dbm->start();

    connect(dbm, SIGNAL(sigWarningMessage(QString)),
            this, SLOT(showWarningMessage(QString)));

    position = ui->paintLabel->pos();
    sun_center.setX(420 - position.x());
    sun_center.setY(352 - 53);

    pixmap = new QPixmap;
    paint = new QPainter;
    
//    path = QDir::currentPath();

//    QString::reverse_iterator slash;
//    uint counter = 0;
//    for (slash = path.rbegin(); slash != path.rend(); ++slash)
//    {
//        if (*slash == '/')
//            break;
//        ++counter;
//    }
//    path.chop(counter - 1);
//    qDebug() << path;

    // Set RGB sliders
    ui->redSlider->setRange(0,255);
    ui->greenSlider->setRange(0,255);
    ui->blueSlider->setRange(0,255);

    ui->redSlider->setValue(255);
    ui->greenSlider->setValue(255);
    ui->blueSlider->setValue(255);

    ui->paintLabel->setPixmap(*pixmap);
    
    // Set RGB LCD
    ui->lcdRed->setPalette(QPalette(Qt::red));
    ui->lcdGreen->setPalette(QPalette(Qt::green));
    ui->lcdBlue->setPalette(QPalette(Qt::blue));

    ui->lcdRed->display(ui->redSlider->value());
    ui->lcdGreen->display(ui->greenSlider->value());
    ui->lcdBlue->display(ui->blueSlider->value());
}

MainWindow::~MainWindow()
{
    qDebug() << "~MainWindow 0";
    thread_dbm->quit();
    thread_dbm->wait();
    delete dbm;
    delete thread_dbm;
    delete cme;
    delete sw;
    delete paint;
    delete pixmap;
    delete ui;
    qDebug() << "~MainWindow 1";
}

// Set longitude of CME
//
void MainWindow::on_longitude_doubleSpinBox_valueChanged(double phi)
{
    cme->setLongitude(phi);
    qDebug() << "CME longitude = " << cme->getLongitude();
}

// Set initial distance between CME and Sun
//
void MainWindow::on_initDistance_SpinBox_valueChanged(double r0)
{
    cme->setInitDistance(r0);
    qDebug() << "CME init distance = " << cme->getInitDistance();
}

// Set initial cone angle of CME
//
void MainWindow::on_initAngle_SpinBox_valueChanged(double alpha)
{
    cme->setConeAngle(alpha);
    qDebug() << "CME init cone angle = " << cme->getConeAngle();
}

// Set initial speed of CME
//
void MainWindow::on_initSpeed_SpinBox_valueChanged(double v0)
{
    cme->setInitSpeed(v0);
    qDebug() << "CME init speed = " << cme->getInitSpeed();
}

// Set initial size of CME
//
void MainWindow::on_initSizeSpinBox_valueChanged(double s0)
{
    cme->setSize(s0);
    qDebug() << "CME init size = " << cme->getSize();
}

// Set points number
//
void MainWindow::on_pointsNumber_spinBox_valueChanged(int N)
{
    dbm->setPointsNumber(N);
//    qDebug() << "Number of point = " << dbm->getPointsNumber();
}

// Set time step
//
void MainWindow::on_TimeStep_SpinBox_valueChanged(double dt)
{
    dbm->setTimeStep(dt);
    qDebug() << "Time step = " << dbm->getTimeStep();
}

// Set wind speed
//
void MainWindow::on_WindSpeed_spinBox_valueChanged(int speed)
{
    sw->setSpeed(speed);
    qDebug() << "wind speed =" << sw->value(0,0);
}

void MainWindow::showWarningMessage(const QString & str)
{
    QMessageBox::warning(this, "Model parameters", str);
    //qDebug() << "MainWindow::showWarningMessage: " << str;
}

void MainWindow::on_calculate_button_clicked()
{
    std::vector<CME_parameters> *tr = dbm->calculate(*cme);
    paintTraectory(*tr);
    qDebug() << "exit calculate";
}

// Draw traectory to SW maps
void MainWindow::paintTraectory(const std::vector<CME_parameters> & tr)
{
//    QString filepath = "G:/stop/180128";
    QString filepath = "./tst/180128";
//  QString filepath = QFileDialog::getExistingDirectory(this,
//                                                         tr("Открыть Каталог"),
//                                                         ".",
//                                                         QFileDialog::ShowDirsOnly);
//                                                         | QFileDialog::DontResolveSymlinks);
    QDir dir(filepath.append("/"));
    QString subdir("cme");
    dir.mkdir(subdir);

    // Get list of v3d_*.jpg pictures
    QStringList filters;
    filters << "v3d_*";
    QStringList files = dir.entryList(filters, QDir::Files, QDir::Name);
    if (files.isEmpty())
        showWarningMessage("Данных по солнечному ветра нема!");

//    paint = new QPainter(pixmap);
    QColor color(Qt::black);
    QPen pen(color);

    int n = files.size();
    unsigned step = tr.size() / n;
    qDebug() << "step = " << step;

    // Paint cme
    for (int i = 0; i != n; ++i)
    {
//        qDebug() << "i = " << i;
       // Get filename and create pixmap
        QString name = files.at(i);
        QString str = filepath + name;
        pixmap->load(str);
        paint->begin(pixmap);
        paint->setPen(pen);
        paint->setBrush(QBrush(color));

        // Polar coordinates to rectangle coordinates
        std::vector<QPointF> points_CME = polarToWidgetCoordinates(tr.at(i*step));
        // paint CME points
        for (auto p : points_CME)
        {
            QRectF r(p, QSize(2,2));
            paint->drawEllipse(r);
        }

        ui->paintLabel->setPixmap(*pixmap);
        QString savepath = filepath + subdir + "/" + name;
        pixmap->save(savepath);
        paint->end();
    }
}

// Rectangle coordinates for painting
std::vector<QPointF> MainWindow::polarToWidgetCoordinates(const CME_parameters & p)
{
    std::vector<QPointF> points;
    try {
        int size = p.points_number();
//      qDebug() << "getCoordinates: size = " << size;
        for (int i = 0; i < size; ++i)
        {
            qreal x = sun_center.x() + p.distance->at(i) * cos(p.phi->at(i)) / 692000 * sunRadius;
            qreal y = sun_center.y() - p.distance->at(i)* sin(p.phi->at(i)) / 692000 * sunRadius;

            points.push_back(QPointF(x,y));
        }
    }
    catch(std::out_of_range) {
        qDebug() << "out_of_range";
        points.clear();
    }
    return points;
}


// set red color value
void MainWindow::on_redSlider_valueChanged(int value)
{
    color.setRed(value);
    brush.setColor(color);
    ui->lcdRed->display(value);
    pixmap->fill(color);
    ui->paintLabel->setPixmap(*pixmap);
}

// set green color value
void MainWindow::on_greenSlider_valueChanged(int value)
{
    color.setGreen(value);
    brush.setColor(color);
    ui->lcdGreen->display(value);
    pixmap->fill(color);
    ui->paintLabel->setPixmap(*pixmap);
}

// set blue color value
void MainWindow::on_blueSlider_valueChanged(int value)
{
    color.setBlue(value);
    brush.setColor(color);
    ui->lcdBlue->display(value);
    pixmap->fill(color);
    ui->paintLabel->setPixmap(*pixmap);
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    QPointF p(event->localPos());
    qDebug() << p;
}

