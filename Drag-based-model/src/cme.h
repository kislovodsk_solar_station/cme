#ifndef ICME_H
//------------------- Interface for class CME ----------------------
//------------------------------------------------------------------------
#define ICME_H

#include "cmeImpl.h"
class CME
{
public:
    explicit CME(double longitude_ = 0, double cone_angle_ = 0, double size_ = 0,
         double init_distance_ = 20, double init_speed_ = 0);
    ~CME();
    inline void setLongitude(double phi)   { cme->setLongitude(phi); }
    inline void setConeAngle(double alpha) { cme->setConeAngle(alpha); }
    inline void setSize(double cme_size)   { cme->setSize(cme_size); }
    inline void setInitDistance(double R)  { cme->setInitDistance(R); }
    inline void setInitSpeed(double V)     { cme->setInitSpeed(V); }

    inline double getLongitude()    const { return cme->getLongitude(); }
    inline double getConeAngle()    const { return cme->getConeAngle(); }
    inline double getSize()         const { return cme->getSize(); }
    inline double getInitDistance() const { return cme->getInitDistance(); }
    inline double getInitSpeed()    const { return cme->getInitSpeed(); }

private:
    CMEimpl *cme;
};

#endif // ICME_H
