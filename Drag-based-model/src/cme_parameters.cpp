#include "cme_parameters.h"

//struct with parameters of CME points
CME_parameters::CME_parameters(const CME &cme, unsigned points_number) : n(points_number)
{
    distance = new std::vector<double>;
    phi = new std::vector<double>;
    radial_speed = new std::vector<double>;
    phi_speed = new std::vector<double>;
    radial_acceleration = new std::vector<double>;
    phi_acceleration = new std::vector<double>;

    distance->reserve(n);
    phi->reserve(n);
    radial_speed->reserve(n);
    phi_speed->reserve(n);
    radial_acceleration->reserve(n);
    phi_acceleration->reserve(n);

    init_cone(cme, n);
//    init_old(cme, n);
}

CME_parameters::CME_parameters(unsigned points_number) : n(points_number)
{
    distance = new std::vector<double>;
    phi = new std::vector<double>;
    radial_speed = new std::vector<double>;
    phi_speed = new std::vector<double>;
    radial_acceleration = new std::vector<double>;
    phi_acceleration = new std::vector<double>;

    distance->reserve(n);
    phi->reserve(n);
    radial_speed->reserve(n);
    phi_speed->reserve(n);
    radial_acceleration->reserve(n);
    phi_acceleration->reserve(n);
}

// copy constructor
CME_parameters::CME_parameters(const CME_parameters &other) : n(other.n)
{
    distance = new std::vector<double>;
    phi = new std::vector<double>;
    radial_speed = new std::vector<double>;
    phi_speed = new std::vector<double>;
    radial_acceleration = new std::vector<double>;
    phi_acceleration = new std::vector<double>;

    distance->reserve(n);
    phi->reserve(n);
    radial_speed->reserve(n);
    phi_speed->reserve(n);
    radial_acceleration->reserve(n);
    phi_acceleration->reserve(n);

    *distance =*other.distance;
    *phi = *other.phi;
    *distance = *other.distance;
    *phi = *other.phi;
    *radial_speed = *other.radial_speed;
    *phi_speed = *other.phi_speed;
    *radial_acceleration = *other.radial_acceleration;
    *phi_acceleration = *other.phi_acceleration;
//    qDebug() << "copy constructor CME_parameters: ";
//    qDebug() << "phi[0] =" << phi->at(0) << ", other.phi[0] =" << other.phi->at(0);
//    qDebug() << "vphi[0] =" << phi_speed->at(0) << ", other.vphi[0] =" << other.phi_speed->at(0);
}

// assignment operator
CME_parameters &CME_parameters::operator=(const CME_parameters &other)
{
    if (&other != this)
    {
        CME_parameters temp(other);
        swap(temp);
    }
    return *this;
}

CME_parameters::~CME_parameters()
{
    delete distance;
    delete phi;
    delete radial_speed;
    delete phi_speed;
    delete radial_acceleration;
    delete phi_acceleration;
}

void CME_parameters::init_cone(const CME &cme, unsigned n)
{
    double longitude = degrees_to_rad * cme.getLongitude();
    double size = sun_radius * cme.getSize();
    double cone_angle = degrees_to_rad * cme.getConeAngle();
    double l0 = sun_radius * cme.getInitDistance();
    double R0 = 0;
    if (cone_angle != 0)
        R0 = size/2 / sin(cone_angle/2);     // init curve radius

    if (n == 1 && size != 0)
        qDebug() << "One point for none-zero size!";
    if (n != 1 && size == 0)
        qDebug() << "A few points for zero size!";

    double fraction = 1;
    for (unsigned i = 0; i != n; ++i)
    {
        if (n != 1)
            fraction = 1 - static_cast<double>(2*i)/(n - 1);

        double alpha_i = cone_angle/2 * fraction;
        double r = sqrt( R0*sin(alpha_i) * R0*sin(alpha_i) + (R0*cos(alpha_i) + l0 - R0) * (R0*cos(alpha_i) + l0 - R0) );
        double p = acos((R0*cos(alpha_i) + l0 - R0) / r);
        if (alpha_i < 0)
            p = -p;

        distance->push_back(r);
        phi     ->push_back(p + longitude);

        double v0 = cme.getInitSpeed();
//         скорость точек меняется по синусоиде от sin(cone_angle/2) до 1
//        double v_i = v0 * speed_sin_distrib(alpha_i);
        double v_i = v0 * speed_line_distrib(alpha_i, cone_angle/2);

        radial_speed->push_back(v_i * cos(alpha_i - p));
        phi_speed   ->push_back(v_i * sin(alpha_i - p) / r);

//        qDebug() << "CME_parameters: r, phi, vr, vphi = " << distance->at(i) << phi->at(i)
//                << radial_speed->at(i) << phi_speed->at(i);
    }
}

void CME_parameters::init_old(CME *cme, unsigned n)
{
    double r0 =         sun_radius * cme->getInitDistance();        // [km]
    double cme_R0 =     sun_radius * cme->getSize();       // [km]
    double alpha_0 =    degrees_to_rad * cme->getConeAngle();  // [radian]
    double tg_phi_0 =   tan(cme_R0 / r0);
    double v_0 =       cme->getInitSpeed();                      // [km/s]

    if (n == 1 && cme_R0 != 0)
        qDebug() << "One point for none-zero radius!";
    if (n != 1 && cme_R0 == 0)
        qDebug() << "A few points for zero radius!";

    double fraction = 1;
    double alpha_i = 0;
    double phi_i = 0;

    for (unsigned i = 0; i != n; ++i)
    {
        if (n != 1)
            fraction = 1 - static_cast<double>(2*i)/(n - 1);

        alpha_i = alpha_0 * fraction;
        phi_i   = atan(tg_phi_0 * fraction);

        distance    ->push_back(r0 / cos(phi_i));
        phi         ->push_back(phi_i);
        radial_speed->push_back(v_0 * cos(alpha_i));
        phi_speed   ->push_back(v_0 * sin(alpha_i) / r0 * cos(phi_i));

        qDebug() << "CME_parameters: r, phi, vr, vphi = " << distance->at(i) << phi->at(i)
                << radial_speed->at(i) << phi_speed->at(i);
    }
}

void CME_parameters::swap(CME_parameters &other)
{
    std::swap(n, other.n);
    std::swap(*distance, *other.distance);
    std::swap(*phi, *other.phi);
    std::swap(*radial_speed, *other.radial_speed);
    std::swap(*phi_speed, *other.phi_speed);
    std::swap(*radial_acceleration, *other.radial_acceleration);
    std::swap(*phi_acceleration, *other.phi_acceleration);
}
