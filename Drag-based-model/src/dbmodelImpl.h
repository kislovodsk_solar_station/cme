//----------------------------------------------------------------------
//---------------------- Drag-based model ------------------------------
//------------ a(r) = - gamma(r)(v(r) - w(r))*|v(r) - w(r)| ------------
//----------------------------------------------------------------------
#ifndef MODEL_H
#define MODEL_H

#include <QDebug>
#include <QSharedPointer>
#include <QRectF>
#include <QThread>
#include "solarwind.h"
#include "cme.h"
#include "cme_parameters.h"

//typedef QSharedPointer<std::vector<CME_parameters> > shared_ptr;

class DBModelImpl : public QObject
{
public:
    explicit DBModelImpl(QObject *parent = 0, CME *cme = 0, SolarWind *sw = 0,
                     unsigned points_number_ = 1, double time_step_ = 0.001);
    ~DBModelImpl();

    void setPointsNumber(unsigned N);
    void setTimeStep(double dt);

    unsigned getPointsNumber() const;
    double getTimeStep()       const;
    double getInitGamma()      const;

    std::vector<CME_parameters> * calculate(const CME &cme);
    std::vector<CME_parameters> *traectory;

private:
    CME * cme;
    SolarWind *sw;
    unsigned points_number;     // number of points in CME
    double time_step;           // integration step by time [days]
    double init_gamma;          // gamma(r0)
    double gamma(double r, double phi);

    void runge_cutt(std::vector<CME_parameters> & traectory);
    CME_parameters runge_cutt_step(const CME_parameters &p);

    // Right side of diff equation
    double g1(const double &r, const double &vr, const double &phi, const double &vphi);
    double g2(const double &r, const double &vr, const double &phi, const double &vphi);
};

#endif // MODEL_H
